const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const St = imports.gi.St;
const Main = imports.ui.main;
const Soup = imports.gi.Soup;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Clutter = imports.gi.Clutter;
const PanelMenu = imports.ui.panelMenu;

const CG_URL = 'https://api.coingecko.com/api/v3/coins/markets?vs_currency=eur&per_page=3&price_change_percentage=7d'

let _httpSession;
const CoinGeckoIndicator = new Lang.Class({
  Name: 'CoinGeckoIndicator',
  Extends: PanelMenu.Button,
  _init: function() {
    this.parent(0.0, "Coin Gecko Indicator", false);
    this.buttonText = new St.Label({
      text: _("Loading..."),
      y_align: Clutter.ActorAlign.CENTER
    });
    this.actor.add_actor(this.buttonText);
    this._refresh();
  },
  _refresh: function() {
    this._loadData();
    this._removeTimeout();
    this._timeout = Mainloop.timeout_add_seconds(60, Lang.bind(this, this._refresh));
    return true;
  },
  _loadData: function() {
    _httpSession = new Soup.Session();
    let message = Soup.Message.new('GET', CG_URL);
    message.request_headers.append("Accept", "application/json");
    _httpSession.queue_message(message, Lang.bind(this, function(_httpSession, message) {
      log("new HTTP request");
      if (message.status_code !== 200) {
        return;
      }
      let json = JSON.parse(message.response_body.data);
      this._refreshUI(json);
    }));
  },
  _refreshUI: function (data) {
    let txt = data[0].symbol.toUpperCase() + ' -> ' + data[0].current_price.toFixed(2) + '€ <'+ data[0].price_change_percentage_7d_in_currency.toFixed(1) +'%> (' + (data[0].market_cap/1000000000).toFixed(1) + 'B) || ';
    txt += data[1].symbol.toUpperCase() + ' -> ' + data[1].current_price.toFixed(2) + '€ <'+ data[1].price_change_percentage_7d_in_currency.toFixed(1) +'%> (' + (data[1].market_cap/1000000000).toFixed(1) + 'B) || ';
    txt += data[2].symbol.toUpperCase() + ' -> ' + data[2].current_price.toFixed(2) + '€ <'+ data[2].price_change_percentage_7d_in_currency.toFixed(1) +'%> (' + (data[2].market_cap/1000000000).toFixed(1) + 'B)';
    this.buttonText.set_text(txt);
  },
  _removeTimeout: function() {
    if (this._timeout) {
      Mainloop.source_remove(this._timeout);
      this._timeout = null;
    }
  },
  stop: function() {
    if (_httpSession !== undefined) {
      _httpSession.abort();
    }
    _httpSession = undefined;
    if (this._timeout) {
      Mainloop.source_remove(this._timeout);
    }
    this._timeout = undefined;
    this.menu.removeAll();
  }
});

function init() {}

let cgMenu;

function enable() {
  log(`Enabling extension ${Me.metadata.uuid}`);
  cgMenu = new CoinGeckoIndicator;
  Main.panel.addToStatusArea('cg-indicator', cgMenu);
}

function disable() {
  log(`Disabling extension ${Me.metadata.uuid}`);
  cgMenu.stop();
  cgMenu.destroy();
}
