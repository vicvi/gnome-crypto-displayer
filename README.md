# Crypto Displayer
Gnome Shell Extension to get updated information on the top 3 cryptocurrencies by market capitalization. The information is displayed in a Panel Menu Button and gets updated every 60 seconds.
### References
 - http://smasue.github.io/gnome-shell-tw
 - https://gjs.guide/extensions/
